#include "BankAccount.h"

BankAccount:: BankAccount(std:: string name):_name(name),_balance(0){}

std:: string BankAccount:: getName()
{
	return _name;
}

double BankAccount:: getBalance()
{
	return _balance;
}

double BankAccount:: withdraw(double amount)
{
	double returnValue;
	if(amount <= _balance)
	{
		_balance -= amount;
		return amount;
	}
	if(amount > _balance)
	{
		returnValue = _balance;
		_balance = 0;
	}
	return returnValue;
}

void BankAccount:: deposit(double amount)
{
	_balance += amount;
}