#ifndef LIMITEDBANKACCOUNT_H
#define LIMITEDBANKACCOUNT_H
#include "BankAccount.h"

class LimitedBankAccount : public BankAccount
{
public:
	LimitedBankAccount(double, std:: string);
	void setLimit(double);
	double getLimit();
	double virtual withdraw(double);
private:
	double _limit;
};

#endif