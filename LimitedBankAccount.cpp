#include "LimitedBankAccount.h"

LimitedBankAccount:: LimitedBankAccount(double limit,std:: string name):BankAccount(name),_limit(limit){}

void LimitedBankAccount:: setLimit(double limit)
{
	_limit = limit;
}

double LimitedBankAccount:: getLimit()
{
	return _limit;
}

double LimitedBankAccount:: withdraw(double amount)
{
	double returnValue;
	if((amount > _limit)&&(amount > _balance))
	{
		if (_limit > _balance)
		{
			returnValue = _balance;
			_balance = 0;
			return returnValue;
		}
		else
		{
			_balance -= _limit;
			return _limit;
		}
	}
	if((amount <= _limit)&&(amount > _balance))
	{
		returnValue = _balance;
		_balance = 0;
		return returnValue;
	}
	if((amount > _limit)&&(amount <= _balance))
	{
		_balance -= _limit;
		return _limit;
	}
	if((amount <= _limit)&&(amount < _balance))
	{
		_balance -= amount;
	}
	return amount;
}