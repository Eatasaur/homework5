#ifndef BAKNACCOUNT_H
#define BANKACCOUNT_H
#include <iostream>

class BankAccount
{
public:
	BankAccount(std:: string);
	std:: string getName();
	double getBalance();
	double virtual withdraw(double);
	void virtual deposit(double);
protected:
	std:: string _name;
	double _balance;
};

#endif