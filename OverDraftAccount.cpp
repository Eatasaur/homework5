#include "OverDraftAccount.h"

OverDraftAccount:: OverDraftAccount(double max, std:: string name):BankAccount(name),_maxOverDraft(max){}

double OverDraftAccount:: getMax()
{
	return _maxOverDraft;
}

//����� ��������� �����.
double OverDraftAccount:: withdraw(double amount)
{
	double returnValue;
	if(amount <= _balance-_maxOverDraft)
	{
		_balance -= amount;
		return amount;
	}
	if(amount > _balance-_maxOverDraft)
	{
		returnValue = _balance;
		_balance = _maxOverDraft ;
	}
	return returnValue;
}