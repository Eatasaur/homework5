#ifndef OVERDRAFTACCOUNT_H
#define OVERDRAFTACCOUNT_H
#include "BankAccount.h"

class OverDraftAccount : public BankAccount
{
public:
	OverDraftAccount(double max, std:: string name);
	double getMax();
	double virtual withdraw(double);
private:
	double _maxOverDraft;
};

#endif