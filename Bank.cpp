#include "LimitedBankAccount.h"
#include "OverDraftAccount.h"
//Should work, but I have no idea why it doesn't.

int main()
{
	int choice = 0,type = 0, regCount = 0, limitedCount = 0, overDraftCount = 0, limit, maxOverDraft, i;
	std:: string name;
	BankAccount* regular[20];
	LimitedBankAccount* limited[20];
	OverDraftAccount* overDraft[20];
	std:: cout <<"Welcome to the geniusly called 'MagshiBank'!"<<std:: endl;
	std:: cout <<"1) Add a new account."<<std::endl;
	std:: cout <<"2) List all accounts, type and balance."<<std::endl;
	std:: cout <<"3)Exit."<<std::endl;
	std:: cin >> choice;
	while(choice != 3)
	{
		if(choice == 1)
		{
			std:: cout <<"Choose the type of account you want to make,"<<std::endl;
			std:: cout <<"1) Regular bank account."<<std::endl;
			std:: cout <<"2) Limited bank account."<<std::endl;
			std:: cout <<"3) Over draft account."<<std::endl;
			std:: cin >> type;
			if(type == 1)
			{
				std:: cout <<"Enter your name, please:";
				scanf("%s",name);
				regular[regCount++] = new BankAccount(name);
			}
			if(type == 2)
			{
				std:: cout <<"Enter your name, please:";
				scanf("%s",name);
				std:: cout <<"Enter the account's limit, please";
				std:: cin >> limit;
				limited[limitedCount++] = new LimitedBankAccount(limit, name);
			}
			if(type == 3)
			{
				std:: cout <<"Enter your name, please:";
				scanf("%s",name);
				std:: cout <<"Enter the account's max over draft, please";
				std:: cin >> maxOverDraft;
				overDraft[overDraftCount++] = new OverDraftAccount(maxOverDraft, name);
			}
		}
		if(choice == 2)
		{
			std:: cout <<"Regular accounts:"<<std:: endl;
			for(i = 0;i<regCount;i++)
			{
				printf("Name: %s, Balance %f",regular[i]->getName(),regular[i]->getBalance());
			}
			std:: cout <<"Limited accounts:"<<std:: endl;
			for(i = 0;i<limitedCount;i++)
			{
				printf("Name: %s, Balance %f",limited[i]->getName(),limited[i]->getBalance());
			}
			std:: cout <<"Over draft accounts:"<<std:: endl;
			for(i = 0;i<overDraftCount;i++)
			{
				printf("Name: %s, Balance %f",overDraft[i]->getName(),overDraft[i]->getBalance());
			}
		}
	}
	system("PAUSE");
	return 0;
}